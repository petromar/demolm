//
//  API.swift
//  DemoLM
//
//  Created by Omar Carrassi on 28/01/2019.
//  Copyright © 2019 Omar Carrassi. All rights reserved.
//

import Foundation

public struct API {
    
    let baseURL = "https://services.lastminute.com"
    var hotelsURL: String { return baseURL + "/mobile/stubs/hotels" }
    
}
