//
//  APIManager.swift
//  DemoLM
//
//  Created by Omar Carrassi on 28/01/2019.
//  Copyright © 2019 Omar Carrassi. All rights reserved.
//

import Alamofire
import Foundation

class APIManager: NSObject {
    
    // MARK: - API DELEGATE TYPES
    
    typealias OperationCompletedWithResultAndDictionaryData = (_ success: Bool, _ error: Error?, _ data: [String: AnyObject]) -> Void
    
    
    // MARK: - SHARED INSTANCE INIT
    
    class var sharedInstance: APIManager {
        struct Singleton {
            static let instance = APIManager()
        }
        
        return Singleton.instance
    }
    
    
    // MARK: - API METHODS
    
    func performRequest(withUrl url: String,
                        method: String = "post",
                        parameters: [String: AnyObject] = [:],
                        headers: [String:String] = [:],
                        completion: @escaping OperationCompletedWithResultAndDictionaryData) {
        
        // Check provided url
        guard url != "" else {
            completion(false, nil, [:])
            return
        }
        
        // Final URL configuration
        var urlComponents = URLComponents(string: url)
        urlComponents?.queryItems = [URLQueryItem]()
        
        // Add each param to GET request url
        if stringToHTTPMethod(method: method) == .get {
            for param in parameters {
                guard let value = param.value as? String else { continue }
                urlComponents?.queryItems?.append(URLQueryItem(name: param.key, value: value))
            }
        }
        
        // Final URL string
        guard let requestUrl = urlComponents?.url else {
            completion(false, nil, [:])
            return
        }
        
        // Request headers
        let headers = ["Accept": "application/json"].merging(headers) { (current, _) in current }
        
        // Start an Alamofire POST request providing parameters and headers
        Alamofire.request(requestUrl, method: stringToHTTPMethod(method: method), parameters: parameters, headers: headers)
            .validate()
            .responseJSON { response in
                print(response)
                
                // Check for response status
                switch response.result {
                case .success:
                    // Get the response value as a Dictionary
                    guard let responseValue = response.result.value as? Dictionary<String, AnyObject> else {
                        completion(false, nil, [:])
                        return
                    }
                    completion(true, nil, responseValue)
                case let .failure(error):
                    var errorResponse: Dictionary<String, AnyObject> = [:]
                    if let responseValue = response.result.value as? Dictionary<String, AnyObject> {
                        errorResponse = responseValue
                    }
                    completion(false, error, errorResponse)
                }
        }
    }
    
    
    // MARK: - UTILS
    
    /*
     * Returns the HTTPMethod object related to the provided string
     */
    func stringToHTTPMethod(method: String) -> HTTPMethod {
        switch method.lowercased() {
        case "get":
            return .get
        case "post":
            return .post
        case "put":
            return .put
        case "patch":
            return .patch
        case "delete":
            return .delete
        default:
            return .get
        }
    }
    
}
