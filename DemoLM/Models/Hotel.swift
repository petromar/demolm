//
//  Hotel.swift
//  DemoLM
//
//  Created by Omar Carrassi on 28/01/2019.
//  Copyright © 2019 Omar Carrassi. All rights reserved.
//

import Foundation
import IGListKit

final class Hotel: NSObject, Decodable {
    
    // MARK: - PROPERTIES
    
    public let id: Int
    public let name: String
    public let location: Location?
    public let stars: Int?
    public let checkIn: TimeRange?
    public let checkOut: TimeRange?
    public let contact: Contact?
    public let images: [String]?
    public let userRating: Double?
    
    
    // MARK: - TYPES
    
    // A struct containing address and coordinates about the current hotel.
    public struct Location: Decodable {
        
        public let address: String
        public let city: String
        public let latitude: Double
        public let longitude: Double
    }
    
    // A struct containing start and end time.
    public struct TimeRange: Decodable {
        
        public let from: String
        public let to: String
    }
    
    // A struct containing hotel's contacts.
    public struct Contact: Decodable {
        
        public let phoneNumber: String
        public let email: String
    }
    
    private enum CodingKeys: String, CodingKey {
        case id, name, location, stars, checkIn, checkOut, contact, images, userRating
    }
    
    // MARK: - LIFE CYCLE METHODS
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        id = try container.decode(Int.self, forKey: .id)
        name = try container.decode(String.self, forKey: .name)
        location = try container.decodeIfPresent(Location.self, forKey: .location)
        stars = try container.decodeIfPresent(Int.self, forKey: .stars)
        checkIn = try container.decodeIfPresent(TimeRange.self, forKey: .checkIn)
        checkOut = try container.decodeIfPresent(TimeRange.self, forKey: .checkOut)
        contact = try container.decode(Contact.self, forKey: .contact)
        images = try container.decodeIfPresent([String].self, forKey: .images)
        userRating = try container.decode(Double.self, forKey: .userRating)
    }
    
}

extension Hotel: ListDiffable {
    
    func diffIdentifier() -> NSObjectProtocol
    {
        return self
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool
    {
        return self === object ? true : self.isEqual(object)
    }
    
}
