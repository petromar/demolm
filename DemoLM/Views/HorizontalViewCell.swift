//
//  HorizontalViewCell.swift
//  DemoLM
//
//  Created by Omar Carrassi on 31/01/2019.
//  Copyright © 2019 Omar Carrassi. All rights reserved.
//

import UIKit

class HorizontalViewCell: UICollectionViewCell {
    
    // MARK: - OUTLETS

    @IBOutlet weak var collectionView: UICollectionView!
    
    
    // MARK: - LIFE CYCLE METHODS
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        collectionView.frame = contentView.frame
    }

}
