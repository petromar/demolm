//
//  DetailSelfSizingViewCell.swift
//  DemoLM
//
//  Created by Omar Carrassi on 31/01/2019.
//  Copyright © 2019 Omar Carrassi. All rights reserved.
//

import UIKit
import MapKit

class DetailSelfSizingViewCell: UICollectionViewCell, MKMapViewDelegate {
    
    // MARK: - OUTLETS
    
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var starImageView1: UIImageView!
    @IBOutlet weak var starImageView2: UIImageView!
    @IBOutlet weak var starImageView3: UIImageView!
    @IBOutlet weak var starImageView4: UIImageView!
    @IBOutlet weak var starImageView5: UIImageView!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var checkInLabel: UILabel!
    @IBOutlet weak var checkOutLabel: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    
    
    // MARK: - LIFE CYCLE METHODS

    override func awakeFromNib() {
        super.awakeFromNib()
        
        ratingLabel.layer.cornerRadius = 8.0
    }
    
    
    // MARK: - RENDERING METHODS
    
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        setNeedsLayout()
        layoutIfNeeded()
        let size = contentView.systemLayoutSizeFitting(layoutAttributes.size)
        var newFrame = layoutAttributes.frame
        // note: don't change the width
        newFrame.size.height = ceil(size.height)
        layoutAttributes.frame = newFrame
        return layoutAttributes
    }
    
    
    // MARK: - MKMAPVIEW DELEGATE METHODS
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard annotation is MKPointAnnotation else { return nil }
        
        let identifier = "Annotation"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
        
        if annotationView == nil {
            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView!.canShowCallout = true
        } else {
            annotationView!.annotation = annotation
        }
        
        return annotationView
    }
    
    
    // MARK: - UTILITY METHODS
    
    func setVisibleStarsTo(number: Int) {
        
        switch number {
        case 1:
            starImageView1.isHidden = false
            starImageView2.isHidden = true
            starImageView3.isHidden = true
            starImageView4.isHidden = true
            starImageView5.isHidden = true
        case 2:
            starImageView1.isHidden = false
            starImageView2.isHidden = false
            starImageView3.isHidden = true
            starImageView4.isHidden = true
            starImageView5.isHidden = true
        case 3:
            starImageView1.isHidden = false
            starImageView2.isHidden = false
            starImageView3.isHidden = false
            starImageView4.isHidden = true
            starImageView5.isHidden = true
        case 4:
            starImageView1.isHidden = false
            starImageView2.isHidden = false
            starImageView3.isHidden = false
            starImageView4.isHidden = false
            starImageView5.isHidden = true
        case 5:
            starImageView1.isHidden = false
            starImageView2.isHidden = false
            starImageView3.isHidden = false
            starImageView4.isHidden = false
            starImageView5.isHidden = false
        default:
            starImageView1.isHidden = true
            starImageView2.isHidden = true
            starImageView3.isHidden = true
            starImageView4.isHidden = true
            starImageView5.isHidden = true
        }
        
    }

}
