//
//  HotelTableViewCell.swift
//  DemoLM
//
//  Created by Omar Carrassi on 29/01/2019.
//  Copyright © 2019 Omar Carrassi. All rights reserved.
//

import UIKit

class HotelTableViewCell: UITableViewCell {
    
    // MARK: - OUTLETS
    
    @IBOutlet weak var thumbImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var starsLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    

    // MARK: - LIFECYCLE METHODS
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    // MARK: - RENDERING METHODS
    
    override func draw(_ rect: CGRect) {
        ratingLabel.layer.cornerRadius = 8.0
        starsLabel.backgroundColor = UIColor(patternImage: UIImage(named: "hotel_star") ?? UIImage())
    }
    
}
