//
//  HorizontalEmbeddedCell.swift
//  DemoLM
//
//  Created by Omar Carrassi on 31/01/2019.
//  Copyright © 2019 Omar Carrassi. All rights reserved.
//

import UIKit

class HorizontalEmbeddedCell: UICollectionViewCell {

    // MARK: - OUTLETS
    
    @IBOutlet weak var imageView: UIImageView!
    
    
    // MARK: - LIFE CYCLE METHODS
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
