//
//  HotelModelController.swift
//  DemoLM
//
//  Created by Omar Carrassi on 28/01/2019.
//  Copyright © 2019 Omar Carrassi. All rights reserved.
//

import Foundation
import Sheeeeeeeeet

protocol HotelModelControllerDelegate {
    func sortingKeyDidChange()
}

class HotelModelController {
    
    // MARK: - API DELEGATE TYPES
    
    typealias OperationCompletedWithResult = (_ success: Bool, _ error: Error?) -> Void
    
    
    // MARK: - PROPERTIES
    
    private let _api: API
    private let _apiManager: APIManager
    private var _hotels: [Hotel]?
    private var _sortingKey: SortingKeys?
    
    public var delegate: HotelModelControllerDelegate?
    
    public var hotels: [Hotel] {
        return self.hotelsSortedByCurrentKey()
    }
    
    private enum SortingKeys: String, CaseIterable {
        case nameAsc = "Name (A-Z)"
        case nameDesc = "Name (Z-A)"
        case starsAsc = "Stars n. (lower)"
        case starsDesc = "Stars n. (higher)"
        case ratingAsc = "Rating (lower)"
        case ratingDesc = "Rating (higher)"
    }
    
    public var sortingActionSheet: ActionSheet {
        var items:[ActionSheetItem] = []
        
        items.append(ActionSheetTitle(title: "Order by:"))
        
        for sortingKey in SortingKeys.allCases {
            items.append(ActionSheetItem(title: sortingKey.rawValue, value: sortingKey))
        }
        
        items.append(ActionSheetCancelButton(title: "Cancel"))
        
        let sheet = ActionSheet(items: items) { sheet, item in
            if let value = item.value as? SortingKeys {
                // Set selected value for current sorting key
                self._sortingKey = value
                
                // Notify delegate
                self.delegate?.sortingKeyDidChange()
            }
        }
        
        return sheet
    }
    
    
    // MARK: - HOTEL LIFE CYCLE METHODS
    
    init(api: API, apiManager: APIManager = .sharedInstance)
    {
        // Inject required properties amd singletons
        _api = api
        _apiManager = apiManager
    }
    
    
    // MARK: - HOTELS DATA FETCHING METHODS
    
    func fetchHotels(completion: @escaping OperationCompletedWithResult)
    {
        // Load access token request
        _apiManager.performRequest(withUrl: _api.hotelsURL,
                                   method: "get")
        { (success, error, data) -> Void in
            
            if success == true {
                do {
                    guard let responseData = data["hotels"] as? [[String:AnyObject]] else {
                        completion(false, error)
                        return
                    }
                    
                    // Store fetched hotels
                    var hotelArray: [Hotel] = []
                    for hotelObj in responseData {
                        // Convert dictionary of hotel object to raw data
                        let hotelData = try JSONSerialization.data(withJSONObject: hotelObj, options: .prettyPrinted)
                        // Convert hotel object data to string
                        if let hotelString = String(data: hotelData, encoding: .utf8) {
                            // Get json raw data from hotel object string
                            let jsonData = hotelString.data(using: .utf8)!
                            // Init new Hotel instance from json raw data
                            let hotel = try JSONDecoder().decode(Hotel.self, from: jsonData)
                            // Add new hotel instance to array
                            hotelArray.append(hotel)
                        }
                    }
                    
                    self._hotels = hotelArray
                    completion(true, nil)
                } catch _ {
                    completion(false, error)
                    return
                }
            } else {
                completion(false, error)
            }
            
        }
    }
    
    
    // MARK: - UTILITY METHODS
    
    func hotelsSortedByCurrentKey() -> [Hotel] {
        
        if let sortKey = _sortingKey {
        
            return _hotels?.sorted(by: { (hotel1, hotel2) -> Bool in
                switch (sortKey) {
                case .nameAsc:
                    return hotel1.name < hotel2.name
                case .nameDesc:
                    return hotel1.name > hotel2.name
                case .starsAsc:
                    guard let star1 = hotel1.stars else { return false }
                    guard let star2 = hotel2.stars else { return false }
                    return star1 < star2
                case .starsDesc:
                    guard let star1 = hotel1.stars else { return false }
                    guard let star2 = hotel2.stars else { return false }
                    return star1 > star2
                case .ratingAsc:
                    guard let rating1 = hotel1.userRating else { return false }
                    guard let rating2 = hotel2.userRating else { return false }
                    return rating1 < rating2
                case .ratingDesc:
                    guard let rating1 = hotel1.userRating else { return false }
                    guard let rating2 = hotel2.userRating else { return false }
                    return rating1 > rating2
                }
            }) ?? []
        } else {
            
            return _hotels?.sorted(by: { (hotel1, hotel2) -> Bool in
                return hotel1.id < hotel2.id
            }) ?? []
            
        }
    }
}
