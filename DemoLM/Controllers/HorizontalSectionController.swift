//
//  HorizontalSectionController.swift
//  DemoLM
//
//  Created by Omar Carrassi on 31/01/2019.
//  Copyright © 2019 Omar Carrassi. All rights reserved.
//

import UIKit
import IGListKit

class HorizontalSectionController:  ListSectionController, ListAdapterDataSource {
    
    // MARK: - PROPERTIES
    
    private var imagesUrlList: [String]?
    private lazy var adapter: ListAdapter = {
        let adapter = ListAdapter(updater: ListAdapterUpdater(),
                                  viewController: self.viewController)
        adapter.dataSource = self
        return adapter
    }()
    
    
    // MARK: - LISTSECTIONCONTROLLER DATASOURCE METHODS
    
    override func sizeForItem(at index: Int) -> CGSize {
        let width = collectionContext?.containerSize.width ?? 0
        let height = (width * 333) / 500 
        return CGSize(width: width, height: height)
    }
    
    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let cell = collectionContext?.dequeueReusableCell(withNibName: "HorizontalViewCell",
                                                                bundle: nil,
                                                                for: self,
                                                                at: index) as? HorizontalViewCell else {
                                                                    fatalError()
        }
        
        adapter.collectionView = cell.collectionView
        return cell
    }
    
    override func didUpdate(to object: Any) {
        if let hotel = object as? Hotel {
            imagesUrlList = hotel.images
        }
    }
    
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        guard let imagesUrlList = imagesUrlList else { return [] }
        return imagesUrlList.map { $0 as ListDiffable }
    }
    
    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        return HorizontalEmbeddedController()
    }
    
    func emptyView(for listAdapter: ListAdapter) -> UIView? {
        return nil
    }

}
