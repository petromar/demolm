//
//  DetailSectionController.swift
//  DemoLM
//
//  Created by Omar Carrassi on 31/01/2019.
//  Copyright © 2019 Omar Carrassi. All rights reserved.
//

import UIKit
import IGListKit
import MapKit

class DetailSectionController: ListSectionController {
    
    // MARK: - PROPERTIES
    
    private var hotel: Hotel?
    
    
    // MARK: - LISTSECTIONCONTROLLER DATASOURCE METHODS
    
    override func numberOfItems() -> Int
    {
        return 1
    }
    
    override func sizeForItem(at index: Int) -> CGSize {
        return CGSize(width: collectionContext!.containerSize.width, height: 500)
    }
    
    override func cellForItem(at index: Int) -> UICollectionViewCell
    {
        guard let cell = collectionContext?.dequeueReusableCell(withNibName: "DetailSelfSizingViewCell",
                                                                bundle: nil,
                                                                for: self,
                                                                at: index) as? DetailSelfSizingViewCell else {
                                                                    fatalError()
        }
        
        guard let hotel = hotel else { return cell }
        
        // Set Address Label
        cell.addressLabel.text = (hotel.location?.address.uppercased() ?? "") + ", " + (hotel.location?.city.uppercased() ?? "")
        
        // Set E-Mail Label
        cell.emailLabel.text = hotel.contact?.email
        
        // Set Phone Label
        cell.phoneLabel.text = hotel.contact?.phoneNumber
        
        // Set Stars Images
        cell.setVisibleStarsTo(number: hotel.stars ?? 0)
        
        // Set Rating Label
        cell.ratingLabel.text = "\(hotel.userRating ?? 0.0)"
        
        // Set Check-In Label
        let checkInText = (hotel.checkIn?.from ?? "") + " - " + (hotel.checkIn?.to ?? "")
        cell.checkInLabel.text = checkInText != " - " ? checkInText : ""
        
        // Set Check-Out Label
        let checkOutText = (hotel.checkOut?.from ?? "") + " - " + (hotel.checkOut?.to ?? "")
        cell.checkOutLabel.text = checkOutText != " - " ? checkOutText : ""
        
        // Set Map
        let hotelCoordinates = CLLocationCoordinate2D(latitude: hotel.location?.latitude ?? 0.0,
                                                      longitude: hotel.location?.longitude ?? 0.0)
        let annotation = MKPointAnnotation()
        annotation.coordinate = hotelCoordinates
        cell.mapView.addAnnotation(annotation)
        cell.mapView.centerCoordinate = hotelCoordinates
        
        return cell
    }
    
    override func didUpdate(to object: Any)
    {
        self.hotel = object as? Hotel
    }

}
