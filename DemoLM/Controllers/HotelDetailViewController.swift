//
//  HotelDetailViewController.swift
//  DemoLM
//
//  Created by Omar Carrassi on 31/01/2019.
//  Copyright © 2019 Omar Carrassi. All rights reserved.
//

import UIKit
import IGListKit

class HotelDetailViewController: UIViewController, ListAdapterDataSource {
    
    // MARK: - OUTLETS
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    // MARK: - PROPERTIES
    
    private lazy var adapter: ListAdapter = {
        return ListAdapter(updater: ListAdapterUpdater(), viewController: self)
    }()
    
    public var hotel: Hotel!
    
    
    // MARK: - LIFE CYCLE METHODS

    override func viewDidLoad() {
        super.viewDidLoad()

        // Add collectionview to the view objects
        view.addSubview(collectionView)
        adapter.collectionView = collectionView
        adapter.dataSource = self
        
        self.title = hotel.name
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        collectionView.frame = view.bounds
    }
    
    
    // MARK: - LISTADAPTERDATASOURCE METHODS
    
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        return [hotel] as [ListDiffable]
    }
    
    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        
        let sectionController = ListStackedSectionController(sectionControllers: [
            HorizontalSectionController(),
            DetailSectionController()
            ])
        sectionController.inset = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
        return sectionController
    }
    
    func emptyView(for listAdapter: ListAdapter) -> UIView? { return nil }

}
