//
//  ViewController.swift
//  DemoLM
//
//  Created by Omar Carrassi on 28/01/2019.
//  Copyright © 2019 Omar Carrassi. All rights reserved.
//

import UIKit
import AlamofireImage

class HotelsListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, HotelModelControllerDelegate {
    
    // MARK: - OUTLETS
    
    @IBOutlet weak var hotelsTableView: UITableView!
    
    
    // MARK: - PROPERTIES

    private var imageDownloader: ImageDownloader!
    private var hotelManager: HotelModelController!
    
    
    // MARK: - USER LIFE CYCLE METHODS
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Init image downloader
        imageDownloader = ImageDownloader(configuration: ImageDownloader.defaultURLSessionConfiguration(),
                                          downloadPrioritization: .fifo,
                                          maximumActiveDownloads: 4,
                                          imageCache: AutoPurgingImageCache())
        
        // Init hotel manager
        hotelManager = HotelModelController(api: API(), apiManager: APIManager.sharedInstance)
        
        // Set self as HotelModelControllerDelegate
        hotelManager.delegate = self
        
        // Register custom cell type
        hotelsTableView.register(UINib(nibName: "HotelTableViewCell", bundle: nil), forCellReuseIdentifier: "HotelTableViewCell")
        
        // Set navigation title image
        let logo = UIImage(named: "lm_group_logo_nav")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
        
        // Retrieve hotel list
        fetchHotels()
    }

    
    // MARK: - TABLEVIEW DATASOURCE METHODS
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hotelManager.hotels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "HotelTableViewCell") as? HotelTableViewCell else {
            fatalError()
        }
        
        let currentHotel = hotelManager.hotels[indexPath.row]
        
        // Set Thumb ImageView
        if let firstImageUrl = currentHotel.images?.first {
            imageDownloader.download(URLRequest(url: URL(string: firstImageUrl)!)) { (response) in
                if let image = response.result.value {
                    cell.thumbImageView.image = image
                    cell.thumbImageView.contentMode = .scaleAspectFit
                    cell.thumbImageView.backgroundColor = UIColor.clear
                } else {
                    cell.thumbImageView.image = UIImage(named: "thumb_placeholder")
                    cell.thumbImageView.contentMode = .center
                    cell.thumbImageView.backgroundColor = UIColor.lightGray
                }
            }
        }
        
        // Set Name
        cell.nameLabel.text = currentHotel.name
        
        // Set City
        cell.cityLabel.text = currentHotel.location?.city
        
        // Set stars
        cell.starsLabel.text = "\(currentHotel.stars ?? 0)"
        
        // Set rating
        cell.ratingLabel.text = "\(currentHotel.userRating ?? 0)"
        
        return cell
    }
    
    private func fetchHotels() {
        // Get hotels data from API
        hotelManager.fetchHotels { (success, error) in
            if success == true {
                
                // Reload tableview data
                self.hotelsTableView.reloadData()
            }
        }
    }
    
    
    // MARK: - TABLEVIEW DELEGATE METHODS
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row % 2 == 0 {
            cell.backgroundColor = UIColor(displayP3Red: 237/255, green: 125/255, blue: 182/225, alpha: 0.1)
        } else {
            cell.backgroundColor = UIColor.clear
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        performSegue(withIdentifier: "showDetailViewController", sender: indexPath)
    }
    
    
    // MARK: - HOTEL MODEL CONTROLLER DELEGATE METHODS
    
    func sortingKeyDidChange() {
        self.hotelsTableView.reloadData()
    }
    
    
    // MARK: - ACTIONS METHODS
    
    @IBAction func sortButtonPressed(_ sender: UIBarButtonItem) {
        hotelManager.sortingActionSheet.present(in: self, from: self.view)
    }
    
    
    // MARK: - NAVIGATION
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "showDetailViewController" {
            
            guard let indexPath = sender as? IndexPath else { return }
            
            if let detailViewController = segue.destination as? HotelDetailViewController {
                detailViewController.hotel = hotelManager.hotels[indexPath.row]
            }
            
        }
    }
    
}

