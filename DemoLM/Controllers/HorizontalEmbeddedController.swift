//
//  HorizontalEmbeddedController.swift
//  DemoLM
//
//  Created by Omar Carrassi on 31/01/2019.
//  Copyright © 2019 Omar Carrassi. All rights reserved.
//

import UIKit
import IGListKit
import AlamofireImage

class HorizontalEmbeddedController: ListSectionController {

    // MARK: - PROPERTIES
    
    private var imageUrl: String?
    private let imageDownloader = ImageDownloader(configuration: ImageDownloader.defaultURLSessionConfiguration(),
                                                  downloadPrioritization: .fifo,
                                                  maximumActiveDownloads: 4,
                                                  imageCache: AutoPurgingImageCache())
    
    
    // MARK: - LISTSECTIONCONTROLLER DATASOURCE METHODS
    
    override func sizeForItem(at index: Int) -> CGSize {
        let width = collectionContext?.containerSize.width ?? 0
        let height = collectionContext?.containerSize.height ?? 0
        return CGSize(width: width, height: height)
    }
    
    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let cell = collectionContext?.dequeueReusableCell(withNibName: "HorizontalEmbeddedCell",
                                                                bundle: nil,
                                                                for: self,
                                                                at: index) as? HorizontalEmbeddedCell else {
                                                                    fatalError()
        }
        
        // Set ImageView
        if let url = URL(string: imageUrl ?? "") {
            imageDownloader.download(URLRequest(url: url)) { (response) in
                if let image = response.result.value {
                    cell.imageView.image = image
                } else {
                    cell.imageView.image = UIImage(named: "thumb_placeholder")
                }
            }
        } else {
            cell.imageView.image = UIImage(named: "thumb_placeholder")
        }
        return cell
    }
    
    override func didUpdate(to object: Any) {
        imageUrl = object as? String
    }
    
}
