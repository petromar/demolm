//
//  DemoLMTests.swift
//  DemoLMTests
//
//  Created by Omar Carrassi on 30/01/2019.
//  Copyright © 2019 Omar Carrassi. All rights reserved.
//

import XCTest

@testable import DemoLM

class DemoLMTests: XCTestCase {
    
    var apiManagerUnderTest: APIManager!
    
    override func setUp() {
        super.setUp()
        apiManagerUnderTest = APIManager.sharedInstance
    }
    
    override func tearDown() {
        apiManagerUnderTest = nil
        super.tearDown()
    }
    
    func testAPICall() {
        let url = "https://services.lastminute.com/mobile/stubs/hotels"
        
        let promise = expectation(description: "")
        
        var responseSuccess: Bool = false
        var responseError: Error?
        var responseData: [String: AnyObject] = [:]
        
        apiManagerUnderTest.performRequest(withUrl: url,
                                           method: "get",
                                           parameters: [:],
                                           headers: [:]) { success, error, data in
                                            responseSuccess = success
                                            responseError = error
                                            responseData = data
                                            
                                            promise.fulfill()
        }
        
        waitForExpectations(timeout: 5, handler: nil)
        
        XCTAssert(responseSuccess)
        XCTAssertNil(responseError, responseError?.localizedDescription ?? "")
        XCTAssertGreaterThan(responseData.count, 0)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
